# .bashrc

PROMPT_COMMAND=''
# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi
if [ -f "$HOME/.etc-bashrc" ]; then
  . "$HOME/.etc-bashrc"
fi

__HOMEBREW_PREFIX=/opt/homebrew
__HOMEBREW_PREFIX_LINUX=/home/linuxbrew/.linuxbrew
if [ ! -x "$__HOMEBREW_PREFIX/bin/brew" ] && [ -x "$__HOMEBREW_PREFIX_LINUX/bin/brew" ]; then
  __HOMEBREW_PREFIX="$__HOMEBREW_PREFIX_LINUX"
fi
eval "$("$__HOMEBREW_PREFIX/bin/brew" shellenv)"
PATH="$HOMEBREW_PREFIX/opt/gnu-sed/libexec/gnubin:$PATH"
PATH="$HOMEBREW_PREFIX/opt/make/libexec/gnubin:$PATH"

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
  PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH="$PATH:$HOME/go/bin:/usr/games:$HOME/.config/emacs/bin:$HOME/google-cloud-sdk/bin"

[ ! -d "$HOME/.krew" ] || export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
__prompt_k8s() {
  k8s_current_context=$(kubectl config current-context 2>/dev/null)
  if [[ $? -eq 0 ]]; then echo -e "(${k8s_current_context}) "; fi
}

function mkwebp {
  INPUT_FILE_PATH="${1:-}"
  if [ ! -f "$INPUT_FILE_PATH" ]; then
    echo "error: file '$INPUT_FILE_PATH' not found" >/dev/stderr
    return 1
  fi
  OUTPUT_FILE_PATH="${INPUT_FILE_PATH%.*}.webp"
  if [ -f "$OUTPUT_FILE_PATH" ]; then
    echo "error: output file '$OUTPUT_FILE_PATH' already exists" >/dev/stderr
    return 1
  fi
  cwebp -q "80" "$INPUT_FILE_PATH" -o "$OUTPUT_FILE_PATH" || return 1
  rm "$INPUT_FILE_PATH"
  echo "replaced '$INPUT_FILE_PATH' with '$OUTPUT_FILE_PATH'"
}

# User specific aliases and functions
export PS1="\e[40m\[\033[38;5;40m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput sgr0)\]\e[40m\[\033[38;5;35m\]${HOSTNAME}\[$(tput sgr0)\]\[\033[38;5;15m\]\e[40m:[\[$(tput sgr0)\]\[\033[38;5;39m\]\e[40m\w\[$(tput sgr0)\]\[\033[38;5;15m\]] \$(cd \$(git rev-parse --show-toplevel 2>/dev/null) && ([ ! -d .git ] || git rev-parse --abbrev-ref HEAD 2>/dev/null)) \e[40m#\[$(tput sgr0)\]\[\033[38;5;126m\]\e[40m\$(__prompt_k8s)\[$(tput sgr0)\]\[\033[38;5;15m\]\e[40m\[$(tput sgr0)\]\e[40m@ \$(date)\[$(tput sgr0)\] \$([[ \$? -eq 0 ]] || echo "🔴")\n🐚$([ "$SHLVL" = "1" ] || echo "($SHLVL)") "
export PS1_NORMAL="$PS1"
export PS1_SHORT="🐚 "

[ -f /run/.containerenv ] &&
  [ -f /run/.toolboxenv ] &&
  export PS1=$(printf "\[\033[35m\]⬢\[\033[0m\] %s" "$PS1") &&
  export PATH="$HOME/.local/share/toolbox-bin:$PATH"
#alias ii-start-sharingio-emacs-session="ssh -tA caleb@sharing.io '~/ii/org/start_session.sh' '~/'"
#alias ii-start-sharingio-tmate-session="tmate -S /tmp/caleb.caleb.iisocket new-session -A -s caleb -n bash"
#amixer -c PCH cset 'name=Headphone Mic Boost Volume' 1

export OCR=podman
export INIT_DEFAULT_REPOS_FOLDER="${HOME}/src"
export GIT_CLONE_STRUCTURED_USE_HOSTNAME=true
export HUMACS_PROFILE=doom
export TERM=screen-256color
export EDITOR=editor
# export KIND_EXPERIMENTAL_PROVIDER=podman
export HELM_EXPERIMENTAL_OCI=1
export BASH_SILENCE_DEPRECATION_WARNING=1
export PYTHONPATH="$(brew --prefix)/lib/python3.12/site-packages"

alias please="sudo"
alias ls="ls --color"
alias e='$EDITOR'
alias k=kubectl
alias cdr="cd \$(git rev-parse --show-toplevel)"
alias dospend="printf '$' ; doctl billing-history list --output json | jq '.billing_history[] | select(.type==\"Invoice\") | [.amount | tonumber | floor] | add' | awk '{sum+=\$0} END{print sum}'"
alias p.n="PS1=\$PS1_NORMAL"
alias p.s="PS1=\$PS1_SHORT"
alias alert='notify-send --urgency=normal -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias :wq=exit
alias :q=exit
alias :x=exit
alias cds="cd \$HOME/src"
alias zj=zellij
alias password-string=uuidgen

unset DOOMDIR EMACSLOADPATH

export GPG_TTY=$(tty)
gpgconf --launch gpg-agent
SSH_AUTH_SOCK="$(ssh-find-agent 2>/dev/null)"
if [ -z "$SSH_AUTH_SOCK" ]; then
  eval "$(ssh-agent)"
fi
export SSH_AUTH_SOCK
if type brew &>/dev/null; then
  HOMEBREW_PREFIX="$(brew --prefix)"
  if [[ -r "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh" ]]; then
    source "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh"
  else
    for COMPLETION in "${HOMEBREW_PREFIX}/etc/bash_completion.d/"*; do
      [[ -r "${COMPLETION}" ]] && source "${COMPLETION}"
    done
  fi
fi

APPS_FOR_BASH_COMPLETION=(
  talosctl
  kn
  cosign
  crane
)
for APP in "${APPS_FOR_BASH_COMPLETION[@]}"; do
  which "${APP}" >/dev/null 2>&1 &&
    source <("${APP}" completion bash)
done
eval "$(direnv hook bash)"

export AWS_VAULT_BACKEND=pass
# export AWS_VAULT_PASS_PASSWORD_STORE_DIR=aws-vault
export AWS_VAULT_PASS_PREFIX='aws-vault/'
